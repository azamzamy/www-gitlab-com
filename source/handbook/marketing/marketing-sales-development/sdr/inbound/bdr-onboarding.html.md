---
layout: markdown_page
title: "BDR Onboarding"
---

---
## On this page
{:.no_toc}

- TOC
{:toc}

---


## Week One 

You should understand these concepts by the end of week one:
* BDR Standards
* BDR Process


## Week Two

You should be able to understand these concepts by the end of week two:
 * Buyer Personas
 * Value Propositions
 * Research
 

## Week Three

You should be able to understand these concepts by the end of week three:
 * Cadence
 * Qualification 
 * Handoff
