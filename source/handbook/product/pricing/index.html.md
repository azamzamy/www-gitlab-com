---
layout: markdown_page
title: Pricing
---

## On this page
{:.no_toc}

- TOC
{:toc}

## Departments

Pricing affects product, marketing, and sales.
Therefore general pricing decisions are made by the CEO.

Product makes most decisions on a day to day basis about what feature should go in what plan based on [the Enterprise Edition Tiers
](https://about.gitlab.com/handbook/product/#enterprise-edition-tiers).
Therefore this pricing page is in the product handbook.

## Four tiers

We have four pricing tiers.
How we make decisions on a day to day basis are specified in [what goes in what version](https://about.gitlab.com/handbook/product/#what-goes-in-what-version).

| Version: | Free | Starter | Premium | Ultimate |
| Per user per year: | $0 | $39 | $199 | $999 |
| [Market segment](https://about.gitlab.com/handbook/sales/#market-segmentation): | SMB | Mid Market | Large | Strategic |
| Buyer: | None | Developers | IT | CIO |
| Main competitor: | None | Atlassian | GitHub | Collabnet |
| Type of sell: | None | Feature | Benefit | Transformation |

## Type of sell

- A feature sell means that people want to buy the extra features. This can be done self-serve.
- A benefit sell means that people buy the business outcomes that come with fully utilizing GitLab. You need case studies, metrics like the [conversational development index](https://docs.gitlab.com/ee/user/admin_area/monitoring/convdev.html), and a quarterly checkin with a technical account manager from customer success to review the status of the adoption plan. A competitive process can include a bake-off to show people are 10x faster in starting new projects with GitLab.
- A transformation sell means that people want to transform as an organization. They want to reduce cycle time with 10x and want us to help them. We do workshops with transformation consultants, and define a complete shared project.

## Hybrid sales model

There is a big price difference between the different tiers (0$, $39, $199, $999 per user per year, a price difference of infinite, 5x, 5x, 5x). For GitLab Inc. the majority of revenue is coming from the large enterprises buying the top two tiers.

Most companies in a similar situation would focus only on the highest tiers.
There will be pressure to increase our lowest tier to for example $99.
But we want to make a our hybrid model work for the following reasons:

1. We want to keep being a [good steward of the open source project](https://about.gitlab.com/stewardship/).
1. The lower two tiers are a scalable way to create future customers.
1. If organization are already using Atlassian JIRA we want to be competitive with BitBucket on pricing. If they buy BitBucket it is hard for us to win them back. If they buy GitLab they discover that it can replace JIRA and over time they might buy a higher tier.

Raising the price of the lowest tier will prevent people from starting with GitLab.
It will raise sort term revenue at the expense of our long term market-share.
Instead of raising prices we should focus on communicating the value of the higher tiers.
This will get easier over time when we introduce more features.
In 2016 sales people focussed on free vs. starter, in 2018 on starter vs. premium, hopefully in 2020 on premium vs. ultimate.

Charging $99 for the lowest tier and discounting aggressively when we're up against BitBucket doesn't work. It is unfair to customer that are not aware of this discount and most customer are self-serve, we never talk to them.

You can also check the viability of the different tiers by the conversion from tier to tier.
For example if 5% people upgrade from free to starter, starter to premium, and premium to ultimate your prices are balanced.
If much more people upgrade from free to starter than from starter to premium then starter is underpriced.
Please note that when a customer chooses starter over premium that is much more visible to us than users not buying at all and this might cause us to focus to much on the first case.

A conversion rate of 5% for installations / organizations should not be confused for 5% of the market.
There are organizations of different sizes and larger ones are more likely to pay and purchase a higher tier.

## Selling per main feature

We tried selling one feature at a time but this was was not feasible.
An improved version of that would be selling 7 main features instead of 3 plans.
Examples of main features would be High Availability, Security, Service Desk, etc.

The advantages are:

1. Gradual upgrading to more expensive features.
1. Pay only for the features you use.
1. Add-ons are a common way of selling this.

The disadvantages are:

1. It is [suboptimal for both the buyer and GitLab Inc.](http://cdixon.org/2012/07/08/how-bundling-benefits-sellers-and-buyers/).
1. It is hard for the buyer to estimate how much of each feature they will need.
1. The complexity lengthens the sales process.
1. For users it is unclear what features they can use.
1. The true-up process becomes more complex.
1. The customer has to administer a process how users can get more features.
1. Features get less usage and therefore the improvements are slower.
1. It is hard to do with a hybrid sales model where there is a 25x difference between the lowest and highest paid plan.

We currently think the disadvantages outweigh the advantages.

## Multiple plans for one customer

We considered selling multiple plans to the same customer, allowing them some users on every plan.

The advantages are:

1. Gradual upgrading to more expensive features per team.
1. Pay only for the features you use.

The disadvantages are:

1. The current plans have a blended price assuming 75% of users should pay for the 5x less expensive plan so plan prices would increase by 2.5x `1/(0.25+(0.75/5))`.
1. It is hard for the buyer to estimate how much of each tier they will need.
1. The complexity lengthens the sales process.
1. For users it is unclear what features they can use.
1. It is not common in the industry, buyers don't expect it, and it isn't a boring solution (a sub-value under our [efficiency value](https://about.gitlab.com/handbook/values/#efficiency)).
1. The true-up process becomes more complex.
1. Some features are can't be disabled on a per user basis, like High Availability (HA).
1. The customer has to administer a process how users can get a higher plan.

We currently think the disadvantages outweigh the advantages.

## True-up pricing

- With true-up pricing the license/sale is never blocking user growth.
- We currently charge 100% for people added during the year because some organizations gave an intentionally too low estimate when we charged 50%. If we technically can count user-days we can make it fair for everyone. But not sure if the technical and sales complexity is worth it.
- We're also doing quarterly true-up on request for larger customers.

## When is a dollar not a dollar?

This is the title of a [great article](https://codingvc.com/when-is-a-dollar-not-a-dollar/) of which we'll apply the 8 points to GitLab below:

1. Cost vs. revenue: we can help both to reduce costs and increase revenue, make sure you align to what the priorities of the prospect are.
1. Principle agent problem: for a VP of Engineering you probably want to highlight our features that give her more visibility over features that save developers time.
1. Existing expense vs. new expense: we can make use of existing budgets, be aware that multiple can apply (dev tools, security, operations, DevOps transformation).
1. Above vs. below discretionary spending limits: one more reason to have multiple pricing tiers.
1. Selling services vs. customized products vs off-the-shelf products: we're selling a high margin product and augment with services when needed to make the customer more successful.
1. Selling to many stakeholders vs. one stakeholder: this is another reason for our multiple tiers, Starter is sold to the single stakeholder of development teams, Ultimate is sold to multiple stakeholders and will need the CIO to enforce the transformation.
1. Monthly vs. upfront payments: that is why we only do yearly upfront, sometimes even multi-year upfront.
1. Selling vs. upselling: this is why we have multiple tiers.
