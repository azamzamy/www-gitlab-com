---
layout: job_page
title: "Federal Strategic Account Leader"
---

This has been moved to [/jobs/federal-strategic-account-leader](https://about.gitlab.com/jobs/federal-strategic-account-leader/).
